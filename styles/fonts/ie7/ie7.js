/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'advanced-visual\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-lowercase': '&#xe630;',
		'icon-circle-plus': '&#xe62f;',
		'icon-number': '&#xe622;',
		'icon-filter': '&#xe623;',
		'icon-arrow-down': '&#xe624;',
		'icon-arrow-up': '&#xe625;',
		'icon-reload-alt': '&#xe609;',
		'icon-reload': '&#xe62c;',
		'icon-screen-full': '&#xe62d;',
		'icon-screen-normal': '&#xe62e;',
		'icon-file-excel': '&#xe626;',
		'icon-calendar': '&#xe627;',
		'icon-link': '&#xe628;',
		'icon-sphere': '&#xe629;',
		'icon-share': '&#xe62a;',
		'icon-build-visual': '&#xe621;',
		'icon-gear': '&#xe62b;',
		'icon-aquire-data': '&#xe620;',
		'icon-data-discovery': '&#xe612;',
		'icon-group': '&#xe60d;',
		'icon-intel-logo': '&#xe60c;',
		'icon-folder': '&#xe601;',
		'icon-cube': '&#xe602;',
		'icon-film': '&#xe603;',
		'icon-chevron-left': '&#xe60e;',
		'icon-chevron-right': '&#xe60f;',
		'icon-stats': '&#xe604;',
		'icon-camera': '&#xe605;',
		'icon-drawer': '&#xe606;',
		'icon-compose': '&#xe607;',
		'icon-question': '&#xe600;',
		'icon-list': '&#xe610;',
		'icon-grid': '&#xe611;',
		'icon-file': '&#xe618;',
		'icon-help': '&#xe619;',
		'icon-browse': '&#xe61f;',
		'icon-question-circle': '&#xf059;',
		'icon-file-text-o': '&#xf0f6;',
		'icon-save': '&#xe61c;',
		'icon-keyboard-arrow-left': '&#xe61d;',
		'icon-keyboard-arrow-right': '&#xe61e;',
		'icon-info': '&#xe61a;',
		'icon-info2': '&#xe61b;',
		'icon-data-connect': '&#xe613;',
		'icon-application': '&#xe614;',
		'icon-data-connection': '&#xe615;',
		'icon-search': '&#xe608;',
		'icon-files': '&#xe60a;',
		'icon-seo': '&#xe60b;',
		'icon-dataset': '&#xe616;',
		'icon-visual': '&#xe617;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
