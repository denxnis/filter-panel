(function() {
	'use strict';

	angular.module('filterPanel', [])
		.directive('barChart', ['$window', '$timeout', '$filter', function($window, $timeout, $filter) {
			return {
				restrict: 'AE',
				replace: false,
				priority: 1,
				scope: {
					chartData: '=',
					dimension: '=',
					refresh: '=',
					options: '=?'
				},
				link: function(scope, element, attrs) {
					//Fine-tune these
					var vOffset = 0.5,
						gHeight = 30,
						barHeight = 10,
						vOffsetTxt = -5,
						marginRight = 22,
						vTxtMargin = 35,
						firstLoad = true; //hack to fix IE issue

					var svg,
						width,
						height,
						widthScale,
						canvas,
						container,
						row,
						keyTxt,
						valTxt,
						outBar,
						inBar,
						maxVal;

					var dimFilters = [];

					var w = angular.element($window);

					if (!scope.options) {
						scope.options = {};
					}

					function init() {
						height = element.parent().height();
						width = element.parent().width() - marginRight;
						dimFilters = [];

						if (scope.dimension) scope.dimension.filterAll();
						if (svg) svg.remove();

						svg = d3.select(element[0]).append('svg')
							.attr('height', height)
							.attr('width', width + marginRight);

						canvas = svg.append('g').attr('class', 'gParent');
					}

					init();

					function drawChart(data) {
						if(scope.options.reset) {
							scope.options.reset = false;
							maxVal = ( scope.options.percentage ? 100 : getMaxVal(scope.chartData) );
							widthScale = d3.scale.linear();
							widthScale.domain([0, maxVal]);
							widthScale.range([0, width]);					
						}

						row = canvas.selectAll('g')
							.data(data);

						container = row.enter()
							.append('g')
							.attr('transform', function(d, i) {
								return 'translate(0,' + (i + vOffset) * gHeight + ')';
							});

						keyTxt = container.append('text')
							.text(function(d) {
								return d.key;
							})
							.attr('x', 0)
							.attr('y', vOffsetTxt)
							.attr('class', 'kTxt')
							.on('click', onTouch)
							.on('touchstart', onTouch);

						valTxt = container.append('text')
							.text(function(d) {
								if (scope.options.percentage) return d.value + '%';
								return d.value;
							})
							.attr('x', width - vTxtMargin)
							.attr('y', vOffsetTxt)
							.attr('class', 'vTxt');

						outBar = container.append('rect')
							.attr('width', function(d) {
								return widthScale(maxVal);
							})
							.attr('height', barHeight)
							.attr('class', 'outBar')
							.on('click', onTouch)
							.on('touchstart', onTouch);

						inBar = container.append('rect')
							.attr('width', function(d) {
								return widthScale(d.value);
							})
							.attr('height', barHeight)
							.attr('class', 'inBar')
							.on('click', onTouch)
							.on('touchstart', onTouch);

						row.exit().remove();

						svg.selectAll('.outBar')
							.data(data)
							.on('click', onTouch)
							.on('touchstart', onTouch);

						//IE Hack
						if (firstLoad) {
							firstLoad = false;
							svg.selectAll('.inBar')
								.data(data)
								.on('click', onTouch)
								.on('touchstart', onTouch)
								.attr('width', function(d) {
									return widthScale(d.value);
								})
								.attr('class', function(d) {
									if (dimFilters.length === 0) return 'inBar';
									else if (dimFilters.indexOf(d.key) >= 0) return 'inBar';
									else return 'inBar inactive';
								});
						} else {
							svg.selectAll('.inBar')
								.data(data)
								.on('click', onTouch)
								.on('touchstart', onTouch)
								.transition().duration(750)
								.attr('width', function(d) {
									return widthScale(d.value);
								})
								.attr('class', function(d) {
									if (dimFilters.length === 0) return 'inBar';
									else if (dimFilters.indexOf(d.key) >= 0) return 'inBar';
									else return 'inBar inactive';
								});
						}

						svg.selectAll('.vTxt')
							.data(data)
							.transition().duration(750)
							.text(function(d) {
								if (scope.options.percentage) return d.value + '%';
								return d.value;
							});

						svg.selectAll('.kTxt')
							.data(data)
							.on('click', onTouch)
							.on('touchstart', onTouch)
							.transition().duration(750)
							.text(function(d) {
								return d.key;
							});

						//svg.attr('height', element.find('.gParent').get(0).getBBox().height); //get total height and update svg
						svg.attr('height', function(){ return (vOffset * gHeight * data.length)*2; });
					}

					function onTouch(d) {
						var inBar = d3.select(this.parentElement).select('.inBar');
						var vTxt = d3.select(this.parentElement).select('.vTxt');
						var kTxt = d3.select(this.parentElement).select('.kTxt');
						var index = dimFilters.indexOf(d.key);

						if (dimFilters.length === 0) svg.selectAll('.inBar').classed('inactive', true);

						if (index === -1) {
							dimFilters.push(d.key);
							inBar.classed('inactive', false);
							vTxt.classed('active', true);
							kTxt.classed('active', true);
							applyFilter();
							console.log('first');
						} else {
							dimFilters.splice(index, 1);
							inBar.classed('inactive', true);
							vTxt.classed('active', false);
							kTxt.classed('active', false);					
							applyFilter();
							console.log('second');
						}

						if (dimFilters.length === 0) svg.selectAll('.inBar').classed('inactive', false);
						scope.options.usrSel = dimFilters;
						if (scope.refresh) scope.refresh();

						scope.$apply();
					}
					//WATCH CHART DATA
					scope.$watch('chartData', function(newValue, oldValue) {
						if (scope.chartData) {
							if (oldValue === undefined || !maxVal || !widthScale) {
								widthScale = d3.scale.linear();
								maxVal = (scope.options.percentage ? 100 : getMaxVal(scope.chartData));
								widthScale.domain([0, maxVal]);
								widthScale.range([0, width]);
							}

							if (scope.options.sortByKey === true) scope.chartData.sort(compare);

							if (scope.options.searchTxt)
								drawChart($filter('filter')(scope.chartData, {
									key: scope.options.searchTxt
								}));
							else drawChart(scope.chartData);
						}
					});
					//WATCH SEARCH TEXT
					scope.$watch('options.searchTxt', function(newValue, oldValue) {
						if (scope.options.searchTxt && scope.chartData) {
							drawChart($filter('filter')(scope.chartData, {
								key: scope.options.searchTxt
							}));
						} else if (scope.options.searchTxt === "") {
							drawChart(scope.chartData);
						}
					});
					//WATCH FILTER ALL
					scope.$watch('options.filterAll', function(newValue, oldValue) {
						if (scope.options.filterAll) {
							scope.options.filterAll = false;
							scope.options.usrSel = [];
							scope.options.searchTxt = undefined;
							canvas.selectAll('.inBar').attr('class', 'inBar');
							canvas.selectAll('text').classed('active', false);
						}
					});
					//WATCH RESET
					scope.$watch('options.reset', function(newValue, oldValue) {
						if (scope.options.reset) init();
					});

					scope.$watch('options.invert', function(newValue, oldValue) {
						if (scope.options.invert) {
							scope.options.invert = false;

							var inactiveRows = svg.selectAll('.inactive');
							var activeText = svg.selectAll('.active');
							var allKeys = scope.chartData.map(function(i) {
								return i.key;
							});
							var usrSel = scope.options.usrSel;

							if (inactiveRows[0].length > 0 && inactiveRows[0].length < allKeys.length) {
								svg.selectAll('.inBar').classed('inactive', true);
								inactiveRows.classed('inactive', false);

								svg.selectAll('text').classed('active', true);
								activeText.classed('active', false);
								

								usrSel = allKeys.filter(function(el) {
									return scope.options.usrSel.indexOf(el) < 0;
								});

								scope.options.usrSel = usrSel;
							}
						}
					});

					scope.$watch('options.usrSel', function(newValue, oldValue) {
						if (scope.options.usrSel) {
							dimFilters = scope.options.usrSel;
							applyFilter();
							if (scope.refresh) scope.refresh();
						}
					});

					scope.$watch(function() {
							return {
								'h': w.height(),
								'w': w.width()
							};
						},
						function(newValue, oldValue) {
							if (isHidden(element[0])) return;
							if (typeof svg !== 'undefined') {
								$timeout(function() {
									width = element.parent().width() - marginRight;
									svg.attr('width', width + marginRight);


									if (widthScale === undefined) return;
									widthScale.range([0, width]);

									svg.selectAll('.inBar')
										.attr('width', function(d) {
											return widthScale(d.value);
										});

									svg.selectAll('.outBar')
										.attr('width', function(d) {
											return widthScale(maxVal);
										});

									svg.selectAll('.vTxt')
										.attr('x', width - vTxtMargin);
								}, 400);
							}
						}, true);

					w.bind('resize', function() {
						scope.$apply();
					});

					function applyFilter() {
						if (dimFilters.length > 0) scope.dimension.filter(function(d) {
							return dimFilters.indexOf(d) > -1;
						});
						else scope.dimension.filterAll();
					}
				}
			};
		}])
		.directive('histogram', ['$window', '$timeout', function($window, $timeout) {
			return {
				restrict: 'AE',
				replace: false,
				scope: {
					chartData: '=',
					dimension: '=',
					refresh: '=',
					options: '=?'
				},
				link: function(scope, element, attrs) {
					var axisHeight = 20,
						bins = 25,
						barSpacing = 1,
						isDateChart;

					var widthScale,
						heightScale,
						canvas,
						svg,
						xAxis,
						histogram,
						data,
						bar,
						brush,
						gBrush,
						ticks,
						xRectPosition; //array containing bar starting position (x)

					var w = angular.element($window);

					var height = element.parent().height() - axisHeight,
						width = element.parent().width();

					svg = d3.select(element[0]).append('svg')
						.attr('width', width)
						.attr('height', height + axisHeight);

					canvas = svg.append('g').attr('class', 'gParent');

					if (!scope.options) scope.options = {};

					/***** WATCH FOR CHART DATA CHANGE *****/
					scope.$watch('chartData', function(newValue, oldValue) {
						if (scope.chartData === undefined) return;

						//VALIDATE DATA 
						if (allNumbers(scope.chartData)) {
							if (scope.options.autoFormat !== false) {
								scope.options.autoFormat = true;
							}							
						} 
						else if (allDates(scope.chartData)) {
							isDateChart = true;
						} else {
							console.warn("Histogram data contains unrecognized data types which may result in unexpected behavior.");
						}

						height = element.parent().height() - axisHeight;
						width = element.parent().width();

						if (oldValue === undefined || !histogram || scope.options.reset) {
							scope.min = Math.min.apply(Math, scope.chartData);
							scope.max = Math.max.apply(Math, scope.chartData);

							histogram = d3.layout.histogram();
							histogram.range([scope.min, scope.max]);
						}

						data = histogram.bins(bins)(scope.chartData);

						if (oldValue === undefined || scope.options.reset) {
							if(scope.options.reset === true) {
								scope.options.reset = false;
								scope.filterAll = true;
							}
														
							heightScale = d3.scale.linear()
								.domain([0, d3.max(data, function(d) { return d.y; })])
								.range([0, height]);

							//Select appropriate scale
							if (isDateChart) {
								widthScale = d3.time.scale();
							} else {
								widthScale = d3.scale.linear();
							}

							widthScale.domain([d3.min(scope.chartData), d3.max(scope.chartData)]);
							widthScale.range([0, width]);

							xAxis = d3.svg.axis()
								.scale(widthScale)
								.orient('bottom');
						
							if (isDateChart) xAxis.ticks(4);

							brush = d3.svg.brush();
							brush.x(widthScale);
							brush.on('brushend', brushended);

							gBrush = svg.append('g').attr('class', 'brush');
							gBrush.call(brush);
							gBrush.selectAll('rect').attr('height', height);

							xRectPosition = data.map(function(d) {
								return d.x;
							});
						}

						bar = canvas.selectAll('.bar')
							.data(data)
							.enter().append('g')
							.attr('class', 'bar')
							.attr('transform', function(d, i) {
								return 'translate(' + widthScale(xRectPosition[i]) + ',' + (height - heightScale(d.y)) + ')';
							});

						bar.append('rect')
							.attr('x', 1)
							.attr('width', width / data.length - barSpacing)
							.attr('height', function(d) {
								return heightScale(d.y);
							})
							.on('click', onTouch)
							.on('touchstart', onTouch);

						svg.append('g')
							.attr('class', 'x axis')
							.attr('transform', 'translate(0,' + height + ')');

						svg.selectAll('.bar')
							.data(data)
							.transition()
							.duration(750)
							.attr('transform', function(d, i) {
								return 'translate(' + widthScale(xRectPosition[i]) + ',' + (height - heightScale(d.y)) + ')';
							});

						svg.selectAll('.bar rect')
							.data(data)
							.transition()
							.duration(750)
							.attr('height', function(d) {
								return heightScale(d.y);
							});

						//AXIS FORMATTING
						if (scope.options.ticks) xAxis.ticks(scope.options.ticks);
						if (scope.options.tickValues) xAxis.tickValues(scope.options.tickValues);
						if (scope.options.tickFormat) xAxis.tickFormat(scope.options.tickFormat);
						if (scope.options.autoFormat === true) formatAxis(xAxis);

						svg.select('g.x.axis').call(xAxis);

						//Logarithm chart only
						if(scope.options.logarithm)
						svg.select('g.x.axis')
							.selectAll('.tick')
							.select('text')
								.each(function(data) {
									d3.select(this).text(function(d){
										return parseInt(Math.exp(d));
									});
								});
					});

					function onTouch(d) {
						if (isDateChart) {
							var startDate = new Date(d[0]),
								endDate = new Date(d[1]);

							if (startDate.getTime() == endDate.getTime()) {
								scope.options.filterAll = true;
							} else {
								scope.dimension.filterRange([startDate, endDate]);
								scope.options.usrSel = d;
								if (scope.refresh) scope.refresh();
							}
						} else if (d[0] === d[1]) {
							scope.options.filterAll = true;
						} else {
							scope.dimension.filterRange(d);
							scope.options.usrSel = d;
							if (scope.refresh) scope.refresh();
						}

						$timeout(function() {
							scope.$apply();
						});
					}

					function brushended() {
						if (!d3.event.sourceEvent) return; // only transition after input

						if (!isDateChart) {
							//Logarithm check
							if(scope.options.logarithm) {
								scope.options.start = Math.round(Math.exp(Number(brush.extent()[0])));
								scope.options.end = Math.round(Math.exp(Number(brush.extent()[1])));
							} else {
								scope.options.start = Math.round(Number(brush.extent()[0]));
								scope.options.end = Math.round(Number(brush.extent()[1]));
							}
							
							$timeout(function() { scope.$apply(); });
						} else if (isDateChart) {
							scope.options.start = brush.extent()[0];
							scope.options.end = brush.extent()[1];

							$timeout(function() {
								scope.$apply();
							});
						}
					}

					/***** WATCH FOR WINDOW RESIZE *****/
					scope.$watch(function() {
						return {
							'h': w.height(),
							'w': w.width()
						};
					}, function(newValue, oldValue) {
						if (isHidden(element[0])) return;
						$timeout(function() {
							height = element.parent().height() - axisHeight;
							width = element.parent().width();

							svg.attr('width', width);
							svg.attr('height', height);

							if (widthScale === undefined || heightScale === undefined) return;
							widthScale.range([0, width]);
							heightScale.range([0, height]);

							xAxis.scale(widthScale);

							//Save Axis data
							if (width <= 300) {
								if (!ticks) ticks = xAxis.ticks()[0]; //save # of ticks if not already done
								xAxis.ticks(5);
							} else if (ticks) {
								xAxis.ticks(ticks);
							}

							svg.select('g.x.axis').call(xAxis);

							//Logarithm chart only
							if(scope.options.logarithm)
							svg.select('g.x.axis')
								.selectAll('.tick')
								.select('text')
									.each(function(data) {
										d3.select(this).text(function(d){
											return parseInt(Math.exp(d));
										});
									});							

							brush.x(widthScale);
							gBrush.call(brush);

							svg.selectAll('.bar')
								.attr('transform', function(d, i) {
									return 'translate(' + widthScale(xRectPosition[i]) + ',' + (height - heightScale(d.y)) + ')';
								});

							svg.selectAll('.gParent rect')
								.attr('width', width / data.length - barSpacing)
								.attr('height', function(d) {
									return heightScale(d.y);
								});

						}, 400);
					}, true);

					//WATCH BEGIN RANGE
					scope.$watch('options.start', function(newValue, oldValue) {
						if (scope.options.start && scope.options.end) {

							if (element[0].hasAttribute('time')) {
								scope.options.end.setFullYear("2001");
								scope.options.end.setMonth("8");
								scope.options.end.setDate("11");
								scope.options.start.setFullYear("2001");
								scope.options.start.setMonth("8");
								scope.options.start.setDate("11");
							}
							//Logarithm check
							if(scope.options.logarithm) brush.extent([Math.log(scope.options.start), Math.log(scope.options.end)]);
							else brush.extent([scope.options.start, scope.options.end]);
							
							gBrush.call(brush);
							onTouch(brush.extent());
						}
					});
					//WATCH END RANGE
					scope.$watch('options.end', function(newValue, oldValue) {
						if (scope.options.start && scope.options.end) {
							if (element[0].hasAttribute('time')) {
								scope.options.end.setFullYear("2001");
								scope.options.end.setMonth("8");
								scope.options.end.setDate("11");
								scope.options.start.setFullYear("2001");
								scope.options.start.setMonth("8");
								scope.options.start.setDate("11");
							}

							//Logarithm check
							if(scope.options.logarithm) brush.extent([Math.log(scope.options.start), Math.log(scope.options.end)]);
							else brush.extent([scope.options.start, scope.options.end]);
							gBrush.call(brush);
							onTouch(brush.extent());
						}
					});
					//WATCH FILTER ALL
					scope.$watch('options.filterAll', function(newValue, oldValue) {
						if (scope.options.filterAll === true) {
							scope.options.filterAll = false;

							scope.dimension.filterAll();
							svg.select('.brush').call(brush.clear());

							scope.options.usrSel = [];
							scope.options.start = undefined;
							scope.options.end = undefined;

							if (scope.refresh) scope.refresh();
							$timeout(function() {
								scope.$apply();
							});
						}
					});
					//WATCH RESET
					scope.$watch('options.reset', function(newValue, oldValue) {
						if (scope.options.reset === true) {
							scope.options.reset = false;
							scope.min = Math.min.apply(Math, scope.chartData);
							scope.max = Math.max.apply(Math, scope.chartData);
							histogram = d3.layout.histogram();
							histogram.range([scope.min, scope.max]);
							scope.filterAll = true;
							$timeout(function() {
								scope.$apply();
							});
						}
					});
					//WATCH USER SELECTION
					scope.$watch('options.usrSel', function(newValue, oldValue) {});

					w.bind('resize', function() {
						scope.$apply();
					});
				}
			};
		}])
		.directive('ngFilter', function() {
			return {
				restrict: 'E',
				replace: true,
				scope: false,
				templateUrl: 'filterPanel.html',
				//templateUrl: 'template.html',
				transclude: true,
				link: function(scope, element, attrs) {},
				controller: ['$scope', '$element', function($scope, $element) {
					$scope.dsplyCnt = true;

					$scope.destroy = function() {
						$element[0].remove();
					};
					$scope.toggle = function() {
						$scope.dsplyCnt = !$scope.dsplyCnt;
					};
				}]
			};
		})
		.directive('owlCarousel', function() {
			return {
				restrict: 'A',
				replace: true,
				link: function(scope, element, attrs) {
					scope.dirContent = function() {
						return element.find('[chart-data]').length;
					};

					var options = {
						items: 3, //4 items above 1000px browser width
						itemsDesktop: [1100, 3], //3 items between 1000px and 901px
						itemsDesktopSmall: [900, 2], //2 betweem 900px and 601px
						itemsTablet: [600, 1], //2 items between 600 and 0
						itemsMobile: [false], //itemsMobile disabled - inherit from itemsTablet option
						mouseDrag: false
					};

					scope.$watch('dirContent()', function(newValue, oldValue) {
						if (newValue > 0) {
							element.owlCarousel(options);
						}
					});
				}
			};
		})
		.directive('barChartAddOn', function() {
			return {
				restrict: 'E',
				replace: true,
				templateUrl: 'barChartAddOn.html',
				scope: {
					options: '='
				},
				controller: ['$scope', '$element', function($scope, $element) {
					$scope.filterAll = function() {
						if ($scope.options === undefined) return;
						$scope.options.filterAll = true;
					};
					$scope.invertSelection = function() {
						if ($scope.options === undefined) return;
						$scope.options.invert = true;
					};
				}]
			};
		})
		.directive('numberAddOn', function() {
			return {
				restrict: 'E',
				replace: true,
				templateUrl: 'numberAddOn.html',
				scope: {
					options: '='
				},
				controller: ['$scope', '$element', function($scope, $element) {
					$scope.filterAll = function() {
						if ($scope.options === undefined) return;
						$scope.options.filterAll = true;
					};
				}]
			};
		})
		.directive('dateAddOn', function() {
			return {
				restrict: 'E',
				replace: false,
				templateUrl: 'dateAddOn.html',
				scope: {
					options: '='
				},
				controller: ['$scope', '$element', function($scope, $element) {
					$scope.filterAll = function() {
						if ($scope.options === undefined) return;
						$scope.options.filterAll = true;
					};
				}]
			};
		})
		.directive('timeAddOn', function() {
			return {
				restrict: 'E',
				replace: true,
				templateUrl: 'timeAddOn.html',
				scope: {
					options: '='
				},
				controller: ['$scope', '$element', function($scope, $element) {
					$scope.filterAll = function() {
						if ($scope.options === undefined) return;
						$scope.options.filterAll = true;
					};
				}]
			};
		});

	//Check if two arrays are equivalent
	function arraysEqual(arr1, arr2) {
		if (arr1.length !== arr2.length) return false;
		for (var i = arr1.length; i--;) {
			if (arr1[i] !== arr2[i]) return false;
		}
		return true;
	}

	//Retrieve the maximum value within a <key,value> array
	function getMaxVal(x) {
		var maxVal = 0;
		for (var i = 0; i < x.length; i++) {
			if (x[i].value > maxVal) maxVal = x[i].value;
		}
		return maxVal;
	}

	//Check if all array values are of type number
	function allNumbers(x) {
		for (var i = 0; i < x.length; i++) {
			if (typeof x[i] != "number")
				return false;
		}
		return true;
	}

	//Check if all array values are of type date
	function allDates(x) {
		for (var i = 0; i < x.length; i++) {
			if (typeof x[i].getMonth !== 'function')
				return false;
		}
		return true;
	}

	//Check if argument is an array
	function isArray(x) {
		if (Object.prototype.toString.call(x) === '[object Array]') return true;
		else return false;
	}

	//Where el is the DOM element you'd like to test for visibility
	function isHidden(el) {
		return (el.offsetParent === null);
	}

	//automatically format axis
	function formatAxis(axis) {
		axis.tickFormat(function(tickVal) {
			if (tickVal % 1 !== 0) xyz();
			return (tickVal >= 1000 || tickVal <= -1000) ? tickVal / 1000 + "K" : tickVal;
		});

		function xyz() {
			axis.ticks(8);
		}
	}

	//sort bart-chart data by key
	function compare(a, b) {
		if (a.key < b.key)
			return -1;
		if (a.key > b.key)
			return 1;
		return 0;
	}

	angular.module('filterPanel').run(['$templateCache', function($templateCache) {
		$templateCache.put('filterPanel.html', '<div class="ui-panel container-fluid"><div class="ui-panel-head row"><h4 class="col-md-3 col-sm-6 col-xs-6">Filter Panel</h4><ul class="list-inline pull-right ui-option-menu"><li><a class="ui-toggle-pane" ng-click="clearAll()"><span class="intelicon-cancel-filter-solid"></span></a></li></ul></div><div class="ui-panel-content row"><div owl-carousel ng-transclude></div></div></div>');
		$templateCache.put('barChartAddOn.html', '<div bar-chart-add-on><div class="search-inner-addon"><span class="intelicon-search"></span><input type="text" placeholder="Search" ng-model="options.searchTxt" class="form-control filter-input" ></div><div class="pull-right"><button type="submit" class="btn btn-success btn-xs search-cancel" ng-click="filterAll()" data-placement="left" title="Clear"><span class="intelicon-cancel-filter-outlined"></span></button><button type="button" class="btn btn-default btn-xs f-invert" ng-click="invertSelection()" data-placement="left" title="Include/Exclude"><span class="intelicon-random"></span><span class="intelicon-minus-max"></span></button></div><div class="clearfix"></div></div>');
		$templateCache.put('numberAddOn.html', '<div number-add-on>	<div class="range-inner-addon"><input type="number" min="{{options.minVal}}" max="{{options.maxVal}}" placeholder="Start" ng-model="options.start" class="form-control range-input" required><input type="number" min="{{options.minVal}}" max="{{options.maxVal}}" placeholder="End"  ng-model="options.end" class="form-control range-input right" required></div><div class="pull-right"><button type="submit" class="btn btn-success btn-xs search-cancel" ng-click="filterAll()" data-placement="left" title="Clear"><span class="intelicon-cancel-filter-outlined"></span></button></div><div class="clearfix"></div></div>');
		$templateCache.put('dateAddOn.html', '<div date-add-on><div class="range-inner-addon"><input type="date" min="{{options.minVal}}" max="{{options.maxVal}}" placeholder="yyyy-mm-dd" ng-model="options.start" class="form-control range-input" required><input type="date" min="{{options.minVal}}" max="{{options.maxVal}}" placeholder="yyyy-mm-dd" ng-model="options.end" class="form-control range-input right" required></div><div class="pull-right"><button type="submit" class="btn btn-success btn-xs search-cancel" ng-click="filterAll()" data-placement="left" title="Clear"><span class="intelicon-cancel-filter-outlined"></span> </button></div><div class="clearfix"></div></div>');
		$templateCache.put('timeAddOn.html', '<div time-add-on>	<div class="range-inner-addon">	  <input type="time" placeholder="hh:mm:ss" ng-model="options.start" class="form-control range-input">	  <input type="time" placeholder="hh:mm:ss"  ng-model="options.end" class="form-control range-input">	</div>	<div class="pull-right">	  <button type="submit" class="btn btn-success btn-xs search-cancel" ng-click="filterAll()" data-placement="left" title="Clear">	  	<span class="intelicon-cancel-filter-outlined"></span> 	  </button>	</div>	<div class="clearfix"></div></div>');
	}]);
})();