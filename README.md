Filter-Panel
============

This is an old project I worked on many years ago. Uploading for archive purposes.
Many of the tools used are outdated!

1. from the terminal `bower install`
2. from the terminal `npm install`
3. from the terminal `npm start`
4. navigate to localhost:8000


![alt tag](https://bytebucket.org/denxnis/filter-panel/raw/1bceda3df46a5a9a3090ee455df78237519fbb67/index.png)
