'use strict';

angular.module('filterPanelApp', ['filterPanel'])
  .controller('MainCtrl', function ($scope, $http, $timeout, $filter) {
      
      $http.get('data/MOCK_DATA.json')
            .success(function(data, status, headers, config) { crossFilterInit(data); })
            .error( function(data, status, headers, config) { console.log("msg: " + msg + "\nCode" + code) });

      var   countryAggregate,
            genderAggregate;

      function crossFilterInit(data) {
            //CROSSFILTER OBJECT
            var people = crossfilter(data);
            
            //DIMENSIONS
            $scope.DIM_country = people.dimension(function(d) { return d.country });
            $scope.DIM_age = people.dimension(function(d) { return d.age });
            $scope.DIM_gender = people.dimension(function(d) { return d.gender });
            $scope.DIM_date = people.dimension(function(d) { return new Date(d.insertTimestamp) }) 
            $scope.DIM_time = people.dimension(function(d) { return dateToTime(d.insertTimestamp) })

            //AGGREGATE DATA
            countryAggregate = $scope.DIM_country.group().reduceCount();
            genderAggregate = $scope.DIM_gender.group().reduceCount();

            //FETCH DATA
            $scope.chartOneData = countryAggregate.top(Infinity);
            $scope.chartTwoData = genderAggregate.top(Infinity);
            $scope.chartThreeData = $scope.DIM_age.top(Infinity).map(function(i) { return i.age });
            $scope.chartFourData = $scope.DIM_date.top(Infinity).map(function(i) { return new Date(i.insertTimestamp) });
            $scope.chartFiveData = $scope.DIM_time.top(Infinity).map(function(i) { return dateToTime(i.insertTimestamp) });
      }

      //UPDATE DATA
      $scope.refresh = function() {
            $scope.chartOneData = countryAggregate.top(Infinity);
            $scope.chartTwoData = genderAggregate.top(Infinity);
            $scope.chartThreeData = $scope.DIM_age.top(Infinity).map(function(i) { return i.age });
            $scope.chartFourData = $scope.DIM_date.top(Infinity).map(function(i) { return new Date(i.insertTimestamp) });
            $scope.chartFiveData = $scope.DIM_time.top(Infinity).map(function(i) { return dateToTime(i.insertTimestamp) });
      }

      //OPTIONAL
      $scope.chartOneOptions = { percentage: false };
      $scope.chartTwoOptions = {};
      $scope.chartThreeOptions = { logarithm: false, minVal: 18, maxVal: 120 };
      $scope.chartFourOptions = { minVal: "2014-06-03", maxVal: "2015-06-02" };
      $scope.chartFiveOptions = {}

      $scope.clearAll = function() {
            $scope.chartOneOptions.filterAll = true;
            $scope.chartTwoOptions.filterAll = true;
            $scope.chartThreeOptions.filterAll = true;
            $scope.chartFourOptions.filterAll = true;
            $scope.chartFiveOptions.filterAll = true;
      }
});

//USE THIS FUNCTION WHEN IMPLEMENTING TIME
function dateToTime(value) {
      var x = new Date(value);
      return new Date('September 11, 2001 ' + x.getHours() + ':' + x.getMinutes() + ':' + x.getSeconds());
}